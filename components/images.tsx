import Image from 'next/image'

export default function Banner() {
  return (
    <>
      <div style={{ position: 'relative', width: '100%', height:'500px' }}>
        <Image
          src="/images/city.webp"
          alt="Charlotte"
          layout="fill"
          objectFit="cover"
        />
        
      </div>
    </>
  )
}

export function Gym() {
  return (
    <>
      <div style={{ position: 'relative', width: '50%', height: '300px' }}>
        <Image
          src="/images/gym.webp"
          alt="Gym"
          layout="fill"
          objectFit="cover"
        />

      </div>
    </>
  )
}

export function Condo() {
  return (
    <>
      <div style={{ position: 'relative', width: '50%', height: '300px' }}>
        <Image
          src="/images/condo.webp"
          alt="Condo"
          layout="fill"
          objectFit="cover"
        />

      </div>
    </>
  )
}

export function Interior1() {
  return (
    <>
      <div style={{ position: 'relative', width: '50%', height: '300px' }}>
        <Image
          src="/images/inside1.webp"
          alt="Inside"
          layout="fill"
          objectFit="cover"
        />

      </div>
    </>
  )
}


export function Interior2() {
  return (
    <>
      <div style={{ position: 'relative', width: '50%', height: '300px' }}>
        <Image
          src= {"/images/inside2.webp"}
          alt="Room"
          layout="fill"
          objectFit="cover"
        />

      </div>
    </>
  )
}






