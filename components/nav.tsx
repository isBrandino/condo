import Head from 'next/head'
export default function Nav() {
  return (
    <>
      <div className="nav">
        <ul>
          <div className="padding-right-links">
            <div className="link">
              <p><a href="/">TheBaba</a></p>
            </div>
          </div>
          <div className="padding-right-links">
            <div className="link">
              <p>(910) 723-4821</p>
            </div>
          </div>
        </ul>
      </div>
      <style jsx>{`
      a{
        color:inherit;
      }

      label{
        margin-right:8px;
      }

      .link {
        float: left;
        width: fit-content;
        align-self: center;
        color: white;
        position: relative;
        margin: 0px 15px 0px;

      }

      .link:hover {
        color: #coral;
      }

      .nav{
        width: 100%;
        margin: 0;
        padding: 0;
        background-color: #000000e6;   
        box-shadow:0px 4px 6px 0px rgba(72, 72, 72, 0.11) ,0px 1px 3px 0px rgba(79, 79, 79, 0.18);
        top: 0;
        left: 0;
        position: sticky;
        z-index: 1;
      }

      .padding-right-links{
        margin-left: 1em;
      }

       p{
         margin: 10px;
       }
    
       ul {
        list-style-type: none;
        margin: 0;
        padding: 5px;
        overflow: hidden;
        font-size: 1.12rem;
        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
          Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
          sans-serif;
      }
      `}</style>
    </>
  )
}
