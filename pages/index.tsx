import Head from 'next/head'
import Banner from '../components/images'
import Nav from '../components/nav'
import { Gym, Condo, Interior1, Interior2 } from '../components/images'

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>TheBaba</title>
        <link rel="icon" href="/icon.ico" />
      </Head>
      <Nav />
      <main>
        <div className="top">
         
          <div className="banners">
            <Banner />
          </div>
        </div>

        <div id="images"/>
        <div className="grid">
          
          <div className="grid-div">
            <p className="description">
              At The baba where quality of life matter. We located in Uptown Charlotte neighborhood.
            </p><br />
            <p className="description">Live in the best rated Condominium on the East Coast.
              Schedule your appointement today. </p>
          </div>
          <div className="grid-div"><Condo />
            <p>Newly Developed living space right in the heart of Uptown</p>
          </div>
          <div className="grid-div"><Gym />
            <p>Indoor Gym and Outdoor Calisthenics Park</p>
          </div>
          <div className="grid-div"><Interior1 />
            <p>Wonderful views</p>
          </div>
          <div className="grid-div"><Interior2 />
            <p>State of the art interior design </p>
          </div>
        </div>

      </main>

      <footer>
        <a
          href="https://isBrandino.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Developed by{' Brandon Dawson '}
        </a>
      </footer>

      <style jsx>{`
        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
   
        a {
          color: inherit;
          text-decoration: none;
        }

        .banners{
          box-shadow:0px 4px 6px 0px rgba(72, 72, 72, 0.11) ,0px 1px 3px 0px rgba(79, 79, 79, 0.18);
          width:100%;
        }

        .container {
          min-height: 100vh;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          background: whitesmoke;
        }
        .description {
          text-align: left;
          position: relative;
          margin: auto auto;
        }

        .grid .description{
          background: #1a426c;
        }

        .description {
          line-height: 2;
          font-size: 1.15rem;
          max-width: 600px;
          animation: fadeIn 1s linear;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;
          max-width: 100%;
          min-width 90vw;
          margin-top: 0rem;
          animation: fadeIn 1s linear;
          moz-animation-duration: 1s;
          -webkit-animation-duration: 1s;
          -moz-animation-name: fadeIn;
          -webkit-animation-name: fadeIn;
        }

        .grid-div {
          background-color: white;
          color: black;
          padding: 20px;
          font-size: 1.25rem;
          text-align: right;
          width: 100%;
          min-height: 8em;
          height: fit-content;
          border-radius: 1px;
          box-shadow: 0px 4px 6px 0px rgba(50,50,93,0.11) , 0px 1px 3px 0px rgba(0,0,0,0.08) 
        }
        .grid-div:nth-child(1){
          background-color: #1a426c;
          color:white;
        }

        .grid-div:nth-child(3), .grid-div:nth-child(5){
          background-color: black;
          color: white;
        }

        footer {
          width: 100%;
          height: 45px;
          display: flex;
          justify-content: center;
          align-items: center;
          background-color: black;
          color: white;
        }

        footer img {
          margin-left: 0.5rem;
        }

        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .logo {
          height: 1em;
        }

        main {
          background: whitesmoke;
          flex: 1;
          margin 1em;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          border: none;
          
        }

        #map {
          height: 100%;
        }

        .top{
          background: whitesmoke;
          width: 100%;
        }
        .title a {
          text-decoration: none;
          border-raduis:1em;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: 1.25rem;
          text-align: center;
        }

        @keyframes fadeIn {
          0% {opacity: 0;}
          100% {opacity: 1;}
       } 

      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
          word-wrap: break-word;
          align-content: center;
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
        }
      `}</style>
    </div>
  )
}
